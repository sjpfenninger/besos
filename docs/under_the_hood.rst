*********************************
C4 diagram of the BESOS platform
*********************************

This image provides a high level overview of the BESOS platform and how the besos library is used in the platform.

.. figure:: images/besosC4.png
   :alt: Image
    Figure 1: C4 Diagram of BESOS
