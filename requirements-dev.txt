-r requirements-complete.txt
black
ipykernel
openpyxl
setuptools
pyKriging
pytest
pytest-cov
pytest-regtest
toolz
xlrd==1.2.0  #Newer version does not support .xlsx
